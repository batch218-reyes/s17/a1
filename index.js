	/*
		[FUNCTION W/ PROMPT]
		1. Create a function which is able to prompt the user to provide their fullname, age, and location.
			-use prompt() and store the returned value into a function scoped variable within the function.
			-display the user's inputs in messages in the console.
			-invoke the function to display your information in the console.
			-follow the naming conventions for functions.
	*/
		

		//first function here:


			function printWelcomeMessage() {
				let fullName = prompt ("Enter your Full name: ");
				let age = prompt("Enter your age: ");
				let location = prompt ("Enter your location: ");

				console.log("hello," + fullName);
				console.log("You are " + age + " years old. ");
				console.log("You lived in" + location);
			}

			printWelcomeMessage();


	/*
		[FUNCTION TO DISPLAY]
		2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
			-invoke the function to display your information in the console.
			-follow the naming conventions for functions.


		
	*/

		//second function here:

		function showFavBands(){
			let favBand1 = "1. Switchfoot";
			let favBand2 = "2. Relient K";
			let favBand3 = "3. Elevation";
			let favBand4 = "4. Bethel";
			let favBand5 = "5. Hillsong";

			console.log(favBand1);
			console.log(favBand2);
			console.log(favBand3);
			console.log(favBand4);
			console.log(favBand5);
		}

		showFavBands();

	/*
		[FUNCTION TO DISPLAY]
		3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
			-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
			-invoke the function to display your information in the console.
			-follow the naming conventions for functions.
		
	*/
		
		//third function here:

			function showFavMovie(){
			let favMovies1 = "1. Iron Man 1";
			let favMovies2 = "2. Spider Man, No way Home";
			let favMovies3 = "3. Avengers: End game";
			let favMovies4 = "4. Thor: Ragnarok";
			let favMovies5 = "5. Naruto: the Last";

			let tomRating1 = "94%";
			let tomRating2 = "93%";
			let tomRating3 = "94%";
			let tomRating4 = "93%";
			let tomRating5 = "82%";
			

			console.log(favMovies1);
			console.log("Rotten Tomato Ratings: " + tomRating1);
			console.log(favMovies2);
			console.log("Rotten Tomato Ratings: " + tomRating2);
			console.log(favMovies3);
			console.log("Rotten Tomato Ratings: " + tomRating3);
			console.log(favMovies4);
			console.log("Rotten Tomato Ratings: " + tomRating4);
			console.log(favMovies5);
			console.log("Rotten Tomato Ratings: " + tomRating5);
		}

		showFavMovie();

/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



printFriends();
function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt ("Enter your first friend's name:" ); 
	let friend2 = prompt ("Enter your second friend's name:"); 
	let friend3 = prompt ("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};